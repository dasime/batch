@ECHO off
REM 	================================================
REM		Program name?
REM			Tape
REM		What does this program do?
REM			This program uses Streamlink to record - tape - an ongoing livestream.
REM			Easiest way to install is with `pip install streamlink`.
REM			Streamlink homepage: https://github.com/streamlink/streamlink
REM		Program version?
REM			Build.01	2020-09-04
REM		Program author?
REM			David Sime
REM 	================================================

streamlink %* best --hls-live-restart -o "%DATE:~0,4%%DATE:~5,2%%DATE:~8,2%_%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%.ts"

@PAUSE
