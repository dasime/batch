@ECHO off
REM 	================================================
REM		Program name?
REM			md5it
REM		What does this program do?
REM			Calculate the md5 sum of a file and rename it that preserving the file extension.
REM		Program version?
REM			Build.03	2016-10-14
REM		Program author?
REM			David Sime
REM 	================================================
IF NOT EXIST "%~dpnx1" GOTO fileReadError
ECHO.

:: We use a funky way of setting the output, so we should blank the variable first
SET "md5sum="
SET filedetails=*%~nx1:!

ECHO Hashing "%~n1" . . .
FOR /f "delims=" %%i IN ('CertUtil -hashfile "%~dpnx1" MD5') DO (
CALL SET md5sum=%%md5sum%%!%%i
)

:: Trim the leading and trailing output, remove the spaces, convert to uppercase
CALL SET md5sum=%%md5sum:%filedetails%=%%
CALL SET md5sum=%%md5sum:!CertUtil: -hashfile command completed successfully.=%%
CALL SET md5sum=%%md5sum: =%%
CALL :UpCase md5sum

ECHO Renaming "%~n1" . . .
REN "%~dpnx1" "%md5sum%%~x1"
GOTO:EOF

:fileReadError
ECHO You need to drag a file onto this batch program in order to continue.
ECHO.
PAUSE
GOTO:EOF

:UpCase
:: Taken from http://www.robvanderwoude.com/battech_convertcase.php
:: Subroutine to convert a variable VALUE to all UPPER CASE.
:: The argument for this subroutine is the variable NAME.
FOR %%i IN ("a=A" "b=B" "c=C" "d=D" "e=E" "f=F" "g=G" "h=H" "i=I" "j=J" "k=K" "l=L" "m=M" "n=N" "o=O" "p=P" "q=Q" "r=R" "s=S" "t=T" "u=U" "v=V" "w=W" "x=X" "y=Y" "z=Z") DO CALL SET "%1=%%%1:%%~i%%"
GOTO:EOF