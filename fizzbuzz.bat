:: >"Write a program that prints the numbers from 1 to 100.
:: >But for multiples of three print 'Fizz' instead of the number and for the multiples of five print 'Buzz'.
:: >For numbers which are multiples of both three and five print 'FizzBuzz'."
@ECHO OFF
SETLOCAL enabledelayedexpansion
FOR /L %%i IN (1,1,100) DO (
	SET /A "b=0"
	SET /A "M3=%%i%%3"
	SET /A "M5=%%i%%5"
	IF [!M3!] EQU [0] (
		<nul (set/p z=Fizz)
		SET /A "!b+=1"
	)
	IF [!M5!] EQU [0] (
		<nul (set/p z=Buzz)
		SET /A "!b+=1"
	)
	IF [!b!] EQU [0] (
		ECHO:%%i
	) ELSE (
		ECHO(
	)
)
ECHO Done fizzbuzzing
@PAUSE