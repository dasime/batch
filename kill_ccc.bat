@ECHO off
REM 	================================================
REM		Program name?
REM			Kill Catalyst Control Centre
REM		What does this program do?
REM			This program kills AMD's Catalyst Control Centre's host and CLI programs.
REM			When CCC freezes. These both need to die before it can start again.
REM			Happens fairly reliably a few hours after switching graphic presets.
REM		Program version?
REM			Build.01	2016-12-09
REM		Program author?
REM			David Sime
REM 	================================================

TASKKILL /F /IM CCC.exe /T
TASKKILL /F /IM CLI.exe /T
@PAUSE