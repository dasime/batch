@ECHO off
REM 	================================================
REM		Program name?
REM			x264_Keyframe_Generator
REM		What does this program do?
REM			This program performs a firstpass encode of a file using x264 to generate a stats file 
REM			that can be used for keyframe accurate timing in Aegisub.
REM		Program version?
REM			Build.01	2013-11-05
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the path to x264.exe
REM		x264		- Where the x264 executable is

	SET x264="C:\Programs\x264\x264_8.exe"

REM		Please set the desired encoding arguements for x264
REM		x264ARGS	- The encoding options you'd like x264 to use

	SET x264ARGS=--slow-firstpass --keyint infinite --pass 1

REM 	================================================

IF NOT EXIST "%~1" GOTO error

SET INPUT=%~n1%~x1

%x264% %x264ARGS% --output NUL "%INPUT%"
PAUSE
GOTO end

:error
ECHO You need to drag an Avisynth script onto this batch program in order to continue.
ECHO.
PAUSE

:end