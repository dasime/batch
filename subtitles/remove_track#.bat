@ECHO off
REM 	================================================
REM		Program name?
REM			Remove_track#
REM		What does this program do?
REM			This program removes the '_track?_???' appended to the end of files by MKVExtract.
REM			If you drag a file onto it, it'll just do that file. If you double click it, it'll rename all .ass files in the current directory.
REM		Program version?
REM			Build.02	2013-07-04
REM		Program author?
REM			David Sime
REM 	================================================

IF NOT EXIST "%~1" GOTO autorun
IF EXIST "%~1" GOTO trim

:autorun
ECHO Removing '_track?_???' from all .ass files in the directory.
FOR /f "delims=" %%f IN ('dir /b *.ass') DO (
call:trim "%%f"
)
GOTO:eof

:trim
SET INPUT=%~n1
SET FILTER=%INPUT:*_track=%
CALL SET OUTPUT=%%INPUT:_track%FILTER%=%%
REN "%INPUT%%~x1" "%OUTPUT%%~x1"
GOTO:eof
