@ECHO off
REM 	================================================
REM		Program name?
REM			autopass
REM		What does this program do?
REM			Creates a VirtualDub job file and executes it to create a passfile.
REM		Program version?
REM			Build.01	2012-12-01
REM		Program author?
REM			T****a & T******g & minor changes by me
REM 	------------------------------------------------
REM		Please set the path to VirtualDub.exe
REM		virtualdub		- Where the VirtualDub executable is

	SET virtualdub="C:\Program Files (x86)\VirtualDub\VirtualDub.exe"

REM 	================================================

@echo off
echo.

IF not exist "%~1" goto error

ECHO directshowsource("%~d1%~p1%~n1%~x1"^) >> "%~d1%~p1%~n1.avs"
ECHO Created %~d1%~p1%~n1.avs

ECHO VirtualDub.audio.SetSource(1); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.audio.SetMode(0); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.audio.SetInterleave(1,500,1,0,0); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.audio.SetClipMode(1,1); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.audio.SetConversion(0,0,0,0,0); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.audio.SetVolume(); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.audio.SetCompression(); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.audio.EnableFilterGraph(0); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.video.SetInputFormat(0); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.video.SetOutputFormat(7); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.video.SetMode(3); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.video.SetSmartRendering(0); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.video.SetPreserveEmptyFrames(0); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.video.SetFrameRate2(0,0,1); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.video.SetIVTC(0, 0, 0, 0); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.video.SetCompression(0x64697678,0,10000,0); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.video.filters.Clear(); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.audio.filters.Clear(); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.project.ClearTextInfo(); >> "%~d1%~p1%~n1.jobs"
ECHO // -- $reloadstop -- >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.RunNullVideoPass(); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.audio.SetSource(1); >> "%~d1%~p1%~n1.jobs"
ECHO VirtualDub.Close(); >> "%~d1%~p1%~n1.jobs"

ECHO Created jobs file
ECHO VirtualDub Starting...

%virtualdub% "%~d1%~p1%~n1.avs" /s "%~d1%~p1%~n1.jobs" /x
move "%~d1%~p1video.pass" "%~d1%~p1%~n1.pass"
del "%~d1%~p1%~n1.jobs"
del "%~d1%~p1%~n1.avs"
ECHO Created passfile
GOTO done


:error
ECHO You need to drag the file over this batch program in order to continue.
ECHO.
PAUSE

:done