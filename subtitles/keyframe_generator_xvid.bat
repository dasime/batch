@ECHO off
REM 	================================================
REM		Program name?
REM			xvid_Keyframe_Generator
REM		What does this program do?
REM			This program performs a firstpass encode of a file using XviD to generate a stats file 
REM			that can be used for keyframe accurate timing in Aegisub.
REM		Program version?
REM			Build.01	2013-11-05
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the path to xvid.exe
REM		xvid		- Where the XviD executable is

::	SET XVID="C:\Programs\MeGUI_2341_x86\tools\xvid_encraw\xvid_encraw.exe"
	SET XVID="C:\Programs\Xvid_132\xvid_encraw.exe"
::	SET XVID="C:\Program Files (x86)\Xvid\xvid_encraw.exe"

REM 	================================================

IF NOT EXIST "%~1" GOTO error
SET INPUT=%~n1%~x1
SET HOUR=%TIME:~0,2%
SET HOUR=%HOUR: =0%
%XVID% -i "%INPUT%" -full1pass -pass1 "%INPUT%_%DATE:~6,4%%DATE:~3,2%%DATE:~0,2%_%HOUR%%TIME:~3,2%%TIME:~6,2%.pass" -progress 10

PAUSE
GOTO end

:error
ECHO You need to drag an Avisynth script onto this batch program in order to continue.
ECHO.
PAUSE

:end