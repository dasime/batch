Keyframe Generator
====
Creating passfiles using XviD via VirtualDub is a nuisance.

Here are some experiments with creating passfiles using XviD and x264 directly.

**WARNING: Only kinda works.**

Disambiguation of VirtualDub's XviD passfile (stats file) generation:

> "Run Video Analysis Pass"
>
> This simply runs the rendering engine with a null output backend. If you have a video codec or filter that generates dummy data in an analysis pass, you can use this command to run the pass without generating a junk file.

Source: http://www.virtualdub.org/blog/pivot/entry.php?id=44

Links
----
* Jawor's Xvid Binaries (XviD download)  
  http://web.archive.org/web/20121002010032/http://jawormat.republika.pl/xvid.html
* information about the x264 log - Doom9's Forum  
  http://forum.doom9.org/showthread.php?t=129491#post1039674
* Explanations of statsfile  
  http://doom10.org/index.php?topic=31.0
