@ECHO off
REM 	================================================
REM		Program name?
REM			Backup Audio Drive
REM		What does this program do?
REM			Backs up the audio drive using rsync.
REM			cwRsync download: https://www.itefix.net/content/cwrsync-free-edition
REM		Program version?
REM			Build.02	2018-12-22
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the path to rsync
REM		rsync	- Where the rsync executeable is

	SET rsync="C:\Programs\cwRsync_5.5.0_x86_Free\bin\rsync.exe"

REM 	------------------------------------------------
REM rsync options explained
REM  -a, --archive               archive mode; equals -rlptgoD (no -H,-A,-X)
REM  /-r, --recursive             recurse into directories
REM  /-l, --links                 copy symlinks as symlinks
REM  /-p, --perms                 preserve permissions
REM  /-t, --times                 preserve modification times
REM  /-g, --group                 preserve group
REM  /-o, --owner                 preserve owner (super-user only)
REM  /-D                          same as --devices --specials
REM  //    --devices               preserve device files (super-user only)
REM  //    --specials              preserve special files
REM

REM  -v, --verbose               increase verbosity
REM  -h, --human-readable        output numbers in a human-readable format
REM  -i, --itemize-changes       output a change-summary for all updates

REM      --delete-excluded       delete files on the exclusion list
REM      --delete                delete extraneous files from destination dirs
REM      --progress              show progress during transfer
REM      --no-perms              Do not preserve permissions. Windows permissions aren't fully supported anyway. Overrides -p implied by -a.

REM  -n, --dry-run               perform a trial run with no changes made
REM 	================================================

SET log=rsync_%DATE:~0,4%%DATE:~5,2%%DATE:~8,2%_%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%.log

SET source=/cygdrive/y/
SET destination=/cygdrive/g/
@ECHO ON

%rsync% -avhin --delete --progress --no-group --log-file="%log%" --exclude-from=rsync-exclude-list.txt %source% %destination%
