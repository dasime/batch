md5it
====
Calculate the MD5 value of a file and rename it that preserving the file extension.

To add this as a command to the right-click menu, you can run this command from an elevated command prompt:

```
REG ADD "HKEY_CLASSES_ROOT\*\shell\Rename MD5 Hash\command" /ve /d "\"C:\Programs\md5sums-1.2\md5it_2.bat\" \"%%1\"" /f
```

Or to manually add it, see this guide: http://www.howtogeek.com/howto/windows-vista/add-open-with-notepad-to-the-context-menu-for-all-files/
