@ECHO OFF

ORIGINAL_FILENAME="original file.mkv"
PATCHED_FILENAME="patched file.mkv"
PATCH_NAME="file patch.xdelta"

IF NOT EXIST "xdelta3.exe" (
ECHO Get xdelta3.exe before continuing.
PAUSE
EXIT
)

ECHO Applying patch . . .

IF EXIST %ORIGINAL_FILENAME% (
xdelta3.exe -d -s %ORIGINAL_FILENAME% %PATCH_NAME% %PATCHED_FILENAME%
ECHO %ORIGINAL_FILENAME% has been successfully patched.
) ELSE (
ECHO No %ORIGINAL_FILENAME% to patch.
)

PAUSE