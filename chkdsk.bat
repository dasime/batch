:: Checks a disk and displays a status report.
::
:: /F              Fixes errors on the disk.
:: /V              On FAT/FAT32: Displays the full path and name of every file
::                 on the disk.
::                 On NTFS: Displays cleanup messages if any.
:: /R              Locates bad sectors and recovers readable information
::                 (implies /F).
:: /X              Forces the volume to dismount first if necessary.
::                 All opened handles to the volume would then be invalid
::                 (implies /F).
:: /B              NTFS only: Re-evaluates bad clusters on the volume
::                 (implies /R)

chkdsk G: /V /X /B
