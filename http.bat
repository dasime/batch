@ECHO off
REM 	================================================
REM		Program name?
REM			http
REM		What does this program do?
REM			Launch a simple HTTP server in the current directory
REM
REM     Run http.reg to add a right-click option to the background of
REM     directories to open an HTTP server in the current directory.
REM   Usage?
REM     http.bat      - Start a sever here on port 8080
REM     http.bat *    - Start a sever here on random port (1000-9999)
REM     http.bat 1212 - Start a sever here on a specific port (1212 in example)
REM		Program version?
REM			Build.01	2021-09-25
REM		Program author?
REM			David Sime
REM 	================================================

SET "port=%1"
IF NOT DEFINED port SET "port=8080"

IF %port%==* SET /A "port=%random% %% 9000 + 1000"

python -m http.server %port%
