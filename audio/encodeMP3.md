Requirements Specification
====
Problem Statement
----
To simplify and reduce the steps required to convert FLAC encoded audio to MP3 on a Windows system.

Background Information
----
There are plenty of software solutions available that can already perform this function but they all suffer from at least one of the following: Function creep, feature creep, software bloat. This has resulted in what should be simple programs becoming excessively complicated and slower.

The case in point and inspiration for this project is an _MeGUI_ installation losing all music transcoding functionality for seemingly no reason.

Environment and System Model
----
The solution is a simple script or program that can perform this function in as few steps as possible. There is potential for expansion on the core target but care should be taken to avoid the three issues mentioned above in relation to existing alternatives. 

When complete an end user should be able to just download the solution along with any dependencies and use it without any further maintenance.

User Interaction
----
While it's not possible to create GUIs for Batch files, it is possible to include options that require user input, or enable the script to accept parameters. In order to create as few steps as possible I'll not be implementing either of these features. Instead, to customise the functionality of the batch file I will make an obvious section at the top of the batch file that the end user can edit, should they need to.

Links
----
- RareWares (LAME download)  
  http://www.rarewares.org/mp3-lame-bundle.php#lame-current
- Xiph (FLAC Download)  
  https://xiph.org/flac/
- Track numbering issue. - Hydrogenaudio Forums  
  http://www.hydrogenaudio.org/forums//index.php?showtopic=95597
