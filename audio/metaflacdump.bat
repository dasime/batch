@ECHO off
REM 	================================================
REM		Program name?
REM			MetaFLACDump
REM		What does this program do?
REM			Dumps FLAC metadata to a file.
REM		Program version?
REM			Build.01	2013-09-30
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the path to METAFLAC
REM		METAFLAC	- Where the METAFLAC executeable is

	SET METAFLAC=C:\Programs\flac-1.2.1-win\bin\metaflac.exe

REM 	================================================

TITLE Displaying Metadata of "%~n1"
ECHO Metadata of "%~n1":
ECHO.
%METAFLAC% --export-tags-to=- "%~dpnx1"
ECHO.
PAUSE