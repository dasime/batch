@ECHO off
REM 	================================================
REM		Program name?
REM			EncodeFLAC_MP3
REM		What does this program do?
REM			This program transcodes audio files from flac to mp3 whilst preserving metadata using FLAC and LAME.
REM			Once you've set the environment variables below, simply drag a flac file onto this batch program to begin.
REM		Program version?
REM			Build.07	2013-11-18
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the paths to METAFLAC, FLAC AND LAME.
REM		METAFLAC	- Where the METAFLAC executable is
REM		FLAC		- Where the FLAC executable is
REM 	LAME		- Where the LAME executable is

	SET METAFLAC=C:\Programs\flac-1.2.1-win\bin\metaflac.exe
	SET FLAC=C:\Programs\flac-1.2.1-win\bin\flac.exe
	SET LAME=C:\Programs\lame3.99.5\lame.exe

REM		Please set the desired encoding arguments for LAME
REM		LAMEARGS	- The encoding options you'd like LAME to use

	SET LAMEARGS=-h -b 320

REM 	================================================

::If passed a file CMD can find continue, if not goto fileReadError
IF NOT EXIST "%~dpnx1" GOTO fileReadError
::If file ends in .flac continue, if not goto incompatibleError
IF NOT %~x1==.flac GOTO incompatibleError
ECHO.
ECHO Retrieving "%~n1" metadata . . .
::Retrieve file metadata using metaflac, iterate through output to set attribute variables
FOR /f "delims=" %%i IN ('C:\Programs\flac-1.2.1-win\bin\metaflac.exe --export-tags-to=- "%~dpnx1"') DO SET %%i
ECHO.
ECHO Starting FLAC to MP3 encode . . .
%FLAC% -d -c "%~dpn1.flac" | %LAME% %LAMEARGS% --tt "%TITLE%" --ta "%ARTIST%" --tl "%ALBUM%" --ty "%DATE%" --tc "%COMMENT%" --tn "%TRACKNUMBER%/%TOTALTRACKS%" --tg "%GENRE%" - "%~dpn1.mp3"
ECHO.
PAUSE
GOTO end

:fileReadError
ECHO You need to drag a flac track onto this batch program in order to continue.
ECHO.
PAUSE
GOTO end

:incompatibleError
ECHO The file you've attempted to encode doesn't appear to be flac.
ECHO This program is only compatible with flac files.
ECHO.
PAUSE

:end
