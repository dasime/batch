@ECHO off
REM 	================================================
REM		Program name?
REM			encodeMonoPhonetoMP3
REM		What does this program do?
REM			This program takes a FLAC file and encodes it as a high quality mono audio MP3. Because my phone only has a mono mic.
REM		Program version?
REM			Build.02	2014-03-12
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the paths to METAFLAC, FLAC AND LAME.
REM		FLAC		- Where the FLAC executable is
REM 	LAME		- Where the LAME executable is

	SET FLAC=C:\Programs\flac-1.2.1-win\bin\flac.exe
	SET LAME=C:\Programs\lame3.99.5\lame.exe

REM		Please set the desired encoding arguments for LAME
REM		LAMEARGS	- The encoding options you'd like LAME to use

	SET LAMEARGS=-h -b 320 -m m -S

REM 	================================================

::If passed a file CMD can find continue, if not goto fileReadError
IF NOT EXIST "%~dpnx1" GOTO fileReadError
ECHO.
IF %~x1==.flac (
ECHO Starting FLAC to MP3 encode . . .
%FLAC% -d -c "%~dpn1.flac" | %LAME% %LAMEARGS% - "%~dpn1.mp3"
ECHO.
PAUSE
GOTO end
) ELSE IF %~x1==.wav (
ECHO Starting WAVE to MP3 encode . . .
ECHO.
%LAME% %LAMEARGS% "%~dpn1.wav" "%~dpn1.mp3"
ECHO.
PAUSE
GOTO end
) ELSE (
::If file ends in .flac continue, if not goto incompatibleError
ECHO The file you've attempted to encode doesn't appear to be flac or wav.
ECHO This program is only compatible with flac and wav files.
ECHO.
PAUSE
GOTO end
)

:fileReadError
ECHO You need to drag a flac track onto this batch program in order to continue.
ECHO.
PAUSE

:end