@ECHO off
REM 	================================================
REM		Program name?
REM			DecodeFLACTAKTTA_WAV
REM		What does this program do?
REM			This program decodes audio files from TTA, TAK or FLAC to WAV.
REM			Once you've set the environment variables below, simply drag an audio file onto this batch program to begin.
REM		Program version?
REM			Build.01	2015-08-15
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the paths to METAFLAC, FLAC AND LAME.
REM		TAK		- Where the TAK executable is
REM		FLAC	- Where the FLAC executable is
REM 	TTA		- Where the TTA executable is

	SET TAK="C:\Programs\TAK_2.3.0\Takc.exe"
	SET TTA=C:\Programs\tta-2.3-64bit-sse4\tta.exe
	SET FLAC=C:\Programs\flac-1.2.1-win\bin\flac.exe

REM 	================================================

::This disgusting nasty was inspired by https://stackoverflow.com/questions/18423443#19795694
CALL :CASE_%~x1
IF ERRORLEVEL 1 CALL :DEFAULT_CASE

ECHO Done.
EXIT /B

:CASE_flac
  %FLAC% -d "%~dpn1.flac" --output-name="%~dpn1_.wav"
  GOTO END_CASE
:CASE_tak
  %TAK% -d -wm1 -md5 "%~dpn1.tak" "%~dpn1_.wav"
  GOTO END_CASE
:CASE_tta
  %TTA% -d "%~dpn1.tta" "%~dpn1_.wav"
  GOTO END_CASE
:DEFAULT_CASE
  ECHO Unknown file. This batch file supports documents ending in .flac, .tak and .tta.
  GOTO END_CASE
:END_CASE
  VER > NUL REM reset ERRORLEVEL
  GOTO :EOF REM return from CALL