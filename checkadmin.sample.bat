@ECHO OFF
:: SFC because https://stackoverflow.com/questions/4051883/batch-script-how-to-check-for-admin-rights/21295806#21295806
sfc | FIND "/SCANNOW" > NUL
IF errorlevel 1 (
   ECHO Failure, please rerun this script from an elevated command prompt. Exiting...
   PAUSE
) ELSE (
   ECHO Success: Admin permissions confirmed.
   PAUSE
)