@ECHO off
REM 	================================================
REM		Program name?
REM			Create_avs
REM		What does this program do?
REM			This program is for the clinically lazy and just creates an AVS skeleton.
REM		Program version?
REM			Build.01	2012-11-13
REM		Program author?
REM			David Sime
REM 	================================================
echo.

IF not exist "%~1" goto error

ECHO DirectShowSource("%~d1%~p1%~n1%~x1"^) >> "%~d1%~p1%~n1.avs"
ECHO Created %~d1%~p1%~n1.avs
PAUSE
GOTO done

:error
ECHO You need to drag the file over this batch program in order to continue.
ECHO.
PAUSE

:done