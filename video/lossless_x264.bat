@ECHO off
REM 	================================================
REM		Program name?
REM			Lossless_x264
REM		What does this program do?
REM			This program is for the clinically lazy and just passes an avisynth script to x264 with valid x264 arguments.
REM			Once you've set the environment variables below, simply drag an avisynth script onto this batch program to begin.
REM			This program is useful for creating intermediate files during complicated filtering.
REM		Program version?
REM			Build.01	2013-04-08
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the path to x264.exe
REM		x264		- Where the x264 executable is

	SET x264="C:\Programs\x264\x264_8.exe"

REM		Please set the desired encoding arguements for x264
REM		x264ARGS	- The encoding options you'd like x264 to use

	SET x264ARGS=--preset placebo -q0

REM 	================================================

IF NOT EXIST "%~1" GOTO error

SET INPUT=%~n1
SET OUTPUT=%INPUT:)= h264)%
ECHO Input: %INPUT%
ECHO Output: %OUTPUT%

%x264% %x264ARGS% --output "%OUTPUT%.mkv" "%INPUT%.avs"
PAUSE
GOTO end

:error
ECHO You need to drag an Avisynth script onto this batch program in order to continue.
ECHO.
PAUSE

:end