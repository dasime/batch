@ECHO off
REM 	================================================
REM		Program name?
REM			PNG CRUSH
REM		What does this program do?
REM			Optimises PNGs, restructuring the colour data and stripping metadata.
REM			Download: http://optipng.sourceforge.net/
REM		Program version?
REM			Build.02	2014-10-27
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the path to optipng.exe
REM		optipng		- Where the optipng executable is

	SET optipng="C:\Programs\optipng-0.7.7-win32\optipng.exe"

REM		Please set the desired compression arguments for optipng
REM		optipngARGS	- The compression options you'd like optipng to use

	SET optipngARGS=-o7 -zm1-9 --clobber -strip all

REM 	================================================

%optipng% %optipngARGS% "%~d1%~p1%~n1.png"
@pause