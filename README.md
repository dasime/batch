David's Batch Scripts
====

_Here Be Dragons_

Other than simple batch files to run other programs, there is pretty much no reason to write complex batch scripts.

Good discussion on this here: https://stackoverflow.com/questions/2569833/what-are-the-reasons-to-use-dos-batch-programs-in-windows

No warranty, not responsible, blah, blah.

Links
----

Useful language stuff

* Reading the output of a command into a batch file variable - The Old New Thing - Site Home - MSDN Blogs  
  https://devblogs.microsoft.com/oldnewthing/20120731-00/?p=7003
* Batch files - The SET command Windows NT 4..Windows 7 Syntax  
  http://www.robvanderwoude.com/ntset.php
* How can you find and replace text in a file using the Windows command-line environment - Stack Overflow  
  http://stackoverflow.com/questions/60034/how-can-you-find-and-replace-text-in-a-file-using-the-windows-command-line-envir/6159108#6159108
* CMD Variable edit replace SS64.com  
  http://ss64.com/nt/syntax-replace.html
* windows - Batch File Resolve Absolute Path from Relative Path andor File Name - Stack Overflow  
  http://stackoverflow.com/questions/1645843/batch-file-resolve-absolute-path-from-relative-path-and-or-file-name
* cmd - Unicode output on Windows command line - Stack Overflow  
  http://stackoverflow.com/questions/1035388/unicode-output-on-windows-command-line
* FLAC - faq: Wildcards on Windows  
  https://xiph.org/flac/faq.html#tools__wildcards_on_windows
