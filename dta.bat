@ECHO off
REM 	================================================
REM		Program name?
REM			DownThemAll
REM		What does this program do?
REM			Grabs all the images in the cache using PuTTY's Secure Copy Module
REM			and Pagent as the key agent.
REM		Program version?
REM			Build.01	2015-08-05
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		PPSCP		- The path to PSCP (PuTTY)
REM		PTARGET		- The username@hostname:path to the screenshots
REM		PDEST		- The destination for the downloads

	SET PPSCP="C:\Program Files (x86)\PuTTY\pscp.exe"
	SET PTARGET="ubuntu@52.208.49.251:/var/chef/runs/chef.tar"
	SET PDEST="%USERPROFILE%\Downloads\dta\"

REM 	================================================

TITLE Grabbing %PTARGET%

ECHO Grabbing %PTARGET% . . .
ECHO.

%PPSCP% -agent %PTARGET% %PDEST%

ECHO.
@PAUSE