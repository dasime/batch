@ECHO off
REM 	================================================
REM		Program name?
REM			Twitch Start
REM		What does this program do?
REM			This program uses Livestreamer to quickly start a Twitch stream.
REM			Once you've set the environment variables below, simply double-click this batch program to begin.
REM		Program version?
REM			Build.03	2016-08-07
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the paths to your media player of choice.
REM		PLAYER	- The path to your mediaplayer

	SET PLAYER="C:\Programs\mpv-x86_64-20150302\mpv.exe"

REM 	================================================
CLS
TITLE Time to Watch Twitch?

ECHO Time to watch Twitch?
ECHO 1 - Trump
ECHO 2 - Forsen
ECHO 3 - Eloise
ECHO 4 - Somebody else
ECHO 5 - No, go away
ECHO.
CHOICE /C 12345 /M "(Select an option from the list above) "
IF ERRORLEVEL 5 GOTO cleanUp
IF ERRORLEVEL 4 GOTO somebodyElse
IF ERRORLEVEL 3 GOTO Eloise
IF ERRORLEVEL 2 GOTO Forsen
IF ERRORLEVEL 1 GOTO Trump

:Trump
TITLE Time to Watch Trump.
SET streamPath= http://www.twitch.tv/trumpsc
GOTO:startStream

:Forsen
TITLE Time to Watch Forsen.
SET streamPath= http://www.twitch.tv/forsenlol
GOTO:startStream

:Eloise
TITLE Time to Watch Eloise.
SET streamPath= http://www.twitch.tv/eloise_ailv
GOTO:startStream

:somebodyElse
TITLE Somebody not listed here
ECHO.
SET /p streamPath= Enter the stream you want to watch (example: http://www.twitch.tv/trumpsc):
GOTO:startStream

:startStream
ECHO.
livestreamer %streamPath% source --player %PLAYER%
ECHO.
@PAUSE
GOTO:cleanUp

:cleanUp
TITLE Command Prompt
GOTO:EOF
