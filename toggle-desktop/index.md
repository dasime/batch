Toggling the Background Colour in Windows 7
====

For several years now I've just had one of two solid colours as my desktop background. But as part of the legacy of having a large collection of wallpapers I've used [Display Fusion](http://www.displayfusion.com/) to manage switching themes. But I only go between solid black for dimming the other monitors, to a burnt orange - and this massive program is really overkill for that.

So I was having a look online for a good way to quickly toggle the colour using a batch file. I found the registry value `HKCU\Control Panel\Colors\Background` contains the colour but quickly discovered that just updating this value with something like `REG ADD "HKCU\Control Panel\Colors" /v Background /f /t REG_SZ /d "0 0 0"` doesn't update the background on Windows 7 until either the `UpdatePerUserSystemParameters` event is fired (https://stackoverflow.com/a/9440226) - which didn't work for me - or the computer is next restarted.

So I put this problem down, Display Fusion was fine for now. Until I remembered `.theme` files store all the profile settings for Windows and you can just double click those to get them to change theme. So the solution here is to read the current background colour, load the theme for whatever isn't already loaded, then close the "Personaiz(s)ation" when it pops up - easy!
