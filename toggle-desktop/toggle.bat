@ECHO OFF
REM 	================================================
REM		Program name?
REM			Toggle Background
REM		What does this program do?
REM			This program retrieves the current background colour and toggles it to the alternative.
REM
REM			You can make a shortcut that contains the following "cmd.exe /c start /min C:\Users\Dave\Desktop\toggle.bat ^& exit"
REM			This will make the batch file open minimised and then close itself after running.
REM				see http://www.computerhope.com/issues/ch000932.htm
REM
REM			Once you've set the environment variables below, double click to begin.
REM		Program version?
REM			Build.02	2016-08-16
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please set the path where the theme files are
REM		themeOne		- Where the first theme is
REM		themeTwo		- Where the second theme is

	SET themeOne="C:\Users\Dave\AppData\Local\Microsoft\Windows\Themes\Orange.theme"
	SET themeTwo="C:\Users\Dave\AppData\Local\Microsoft\Windows\Themes\Black.theme"

REM		Please set the solid colour used in themeOne
REM		backgroundOne	- The solid colour used in themeOne

	SET backgroundOne="128 64 0"

REM 	================================================

SET KEY_NAME="HKCU\Control Panel\Colors"
SET VALUE_NAME=Background

:: Query the registry, save the current colour as a variable.
:: Based on here - https://stackoverflow.com/a/445323
FOR /F "usebackq skip=2 tokens=1-6" %%A IN (`REG QUERY %KEY_NAME% /v %VALUE_NAME% 2^>nul`) DO (
    SET ValueName="%%C %%D %%E"
)

:: If the current colour is themeOne, load themeTwo. Otherwise load themeOne.
IF %ValueName% == %backgroundOne% (
	%themeTwo%
) ELSE (
	%themeOne%
)

GOTO:EOF
