@ECHO off
REM 	================================================
REM		Program name?
REM			internetalert
REM		What does this program do?
REM			Pings Google's DNS, if it makes a connection, executes a command.
REM			Useful for when you're waiting for your internet to come back up.
REM		Program version?
REM			Build.01	2014-11-02
REM		Program author?
REM			David Sime
REM 	------------------------------------------------
REM		Please specify a command to run to celebrate reconnection
REM		CELEBRATE - how to celebrate reconnection

	SET CELEBRATE="C:\Program Files (x86)\Combined Community Codec Pack\MPC\mpc-hc.exe" "C:\Users\Dave\Desktop\03 DISCOTHEQUE.mp3"

REM 	================================================
CLS
SET /a i=1
ECHO Starting to poll for Internet connection . . .
ECHO.
:loop
ECHO Pinging Google DNS (attempt %i%). . .
PING -n 1 8.8.8.8 | find "TTL"
IF errorlevel 1 (
ping -n 1 8.8.8.8 -w 30000 >NUL
SET /a i+=1
GOTO:loop
) ELSE (
ECHO Connection discovered!
%CELEBRATE%
)